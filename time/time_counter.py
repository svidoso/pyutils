
import time

class TimeCounter():

    def __init__(self):
        self.start = int(round(time.time() * 1000))

    def get_elapsed_ms(self):
        return int(round(time.time() * 1000)) - self.start

    def get_elapsed_s(self):
        return self.get_elapsed_ms()/1000.0

    def get_reset_ms(self):
        d = self.get_elapsed_ms()
        self.start = int(round(time.time() * 1000))
        return d

    def get_reset_s(self):
        return self.get_reset_ms()/1000.0

if __name__ == "__main__":
    tc = TimeCounter()
    time.sleep(1)
    print(tc.get_elapsed_ms(), 'ms')