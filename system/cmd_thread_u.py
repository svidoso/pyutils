import os
import sys
import re
if sys.version_info >= (3, 0):
    import _thread
else:
    import thread
import time

def cmd_thread(command, wait=0):

    def run_(command, wait):
        time.sleep(wait)
        os.system(command)

    try:
        if sys.version_info >= (3, 0):
            _thread.start_new(run_, (command,wait))
        else:
            thread.start_new(run_, (command, wait))
    except (NameError,) as e:
        print(re.findall("name '(\w+)' is not defined", str(e))[0])
    except:
        print("error in cmd_thread:")
        print("Unexpected error:" + str(sys.exc_info()[0]))
