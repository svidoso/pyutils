import numpy as np
import numpy.random as rand
import matplotlib.pyplot as plt

n=20

y = rand.binomial(n,0.5, 100000)
y = np.histogram(y, n)
print(y)

plt.plot(y[1])
plt.grid()
plt.show()
