import collections
import os
from pyutils.media.audio_u import read_wave, write_wave
import webrtcvad

import logging
logging.basicConfig(format='%(levelname)s:%(asctime)s %(filename)s::%(funcName)s: %(message)s', datefmt='%d.%m %H:%M', level=logging.DEBUG)
log = logging.getLogger(__name__)

class Frame(object):
    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration


def frame_generator(frame_duration_ms, audio, sample_rate):
    n = int(sample_rate * (frame_duration_ms/1000.0) * 2)
    offset = 0
    timestamp = 0.0
    #duration = (float(n) / sample_rate) / 2.0
    while offset + n < len(audio):
        yield Frame(audio[offset:offset + n], timestamp, frame_duration_ms)
        timestamp += frame_duration_ms
        offset += n


def vad_collector(sample_rate, frame_duration_ms,
                  padding_duration_ms, vad, frames,
                  extract_audio=True):
    num_padding_frames = int(padding_duration_ms / frame_duration_ms)
    ring_buffer = collections.deque(maxlen=num_padding_frames)
    triggered = False
    voiced_frames = []
    for frame in frames:
        #log.debug('1' if vad.is_speech(frame.bytes, sample_rate) else '0')

        if not triggered: # 1. until there are enough successive spoken frames
            ring_buffer.append(frame)
            num_voiced = len([f for f in ring_buffer
                              if vad.is_speech(f.bytes, sample_rate)])
            if num_voiced > 0.9 * ring_buffer.maxlen:
                triggered = True
                voiced_frames.extend(ring_buffer)
                ring_buffer.clear()
        else: # 2. until there are enough successive unspoken frames, join and than go back to 1
            voiced_frames.append(frame)
            ring_buffer.append(frame)
            num_unvoiced = len([f for f in ring_buffer
                                if not vad.is_speech(f.bytes, sample_rate)])
            if num_unvoiced > 0.9 * ring_buffer.maxlen:
                triggered = False
                if extract_audio:
                    yield b''.join([f.bytes for f in voiced_frames])
                else:
                    yield [voiced_frames[0].timestamp, len(voiced_frames)*voiced_frames[0].duration]
                ring_buffer.clear()
                voiced_frames = []
    if voiced_frames:
        if extract_audio:
            yield b''.join([f.bytes for f in voiced_frames])
        else:
            yield [voiced_frames[0].timestamp, len(voiced_frames)*voiced_frames[0].duration]


def detect_va(wav_file_path, agressiveness=1, frame_duration_ms=30, padding_duration_ms=300):
    """
    
    :param audio: 
    :param sample_rate: 
    :param agressiveness: 
    :param frame_duration_ms: 
    :return: List with [stamp, duration] - tuples of detected speech intervals
    """
    log.debug("Frame duration: %ims" % frame_duration_ms)
    log.debug("Padding duration: %ims" % padding_duration_ms)

    audio, sample_rate = read_wave(wav_file_path)

    vad = webrtcvad.Vad(int(agressiveness))
    frames = frame_generator(frame_duration_ms, audio, sample_rate)
    frames = list(frames)
    segments = vad_collector(sample_rate, frame_duration_ms, padding_duration_ms, vad, frames, extract_audio=False)
    return [i for i in segments]

def extract_va(wav_file_path, out_dir, agressiveness=1, frame_duration_ms=30, padding_duration_ms=300):

    """
    Write detected speech intervals to out_dir directory
    :param wav_file_path: 
    :param out_dir: 
    :param agressiveness: 
    :param frame_duration_ms: 
    :param padding_duration_ms: 
    :return: 
    """

    audio, sample_rate = read_wave(wav_file_path)

    vad = webrtcvad.Vad(int(agressiveness))
    frames = frame_generator(frame_duration_ms, audio, sample_rate)
    frames = list(frames)
    segments = vad_collector(sample_rate, frame_duration_ms, padding_duration_ms, vad, frames, extract_audio=True)

    for i, segment in enumerate(segments):
        path = os.path.join(out_dir, 'chunk-%002d.wav' % (i,))
        log.debug(' Writing %s' % (path,))
        write_wave(path, segment, sample_rate)