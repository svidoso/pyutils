#!/usr/bin/python3

# http://stackoverflow.com/questions/20953273/install-opencv-for-python-3-3

from threading import Thread

import cv2


def play_video(fname, print_info=False, async=False):

    """    
    :param fname: 
    :param print_info: 
    :param async: if true, will be played in a new thread == is not blocking
    :return: 
    """

    def _play(fname, print_info):

        cap = cv2.VideoCapture(fname)

        if(print_info):
            print("openCV version: " + cv2.__version__)
            # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

        # With webcam get(CV_CAP_PROP_FPS) does not work.
        # Let's see for ourselves.
        fps = 0
        if int(major_ver) < 3:
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            if print_info:
                print ("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
        else:
            fps = cap.get(cv2.CAP_PROP_FPS)
            if print_info:
                print ("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))

        millisWait = int(1000.0/fps)

        if print_info:
            print("file opened: " + str(cap.isOpened()))

        while(cap.isOpened()):
            ret, frame = cap.read()

            cv2.imshow('frame',frame)
            cv2.waitKey(millisWait)

        cap.release()
        cv2.destroyAllWindows()

    if async:
        thread = Thread(target=_play, args=(fname, print_info))
        thread.start()
    else:
        _play(fname, print_info)

#test

# fname = '/home/schulzet/Projekte/IDEA_projekte/ubrecsys/test_data/activation-log-timodemo/Aktivierungsvideo-17.05.2017_14-29-07.325.mp4'
#
# play_video(fname, async=True)
#
# while 1:
#     pass