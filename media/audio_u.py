
import numpy as np
import wave
from scipy.signal import resample
import contextlib

def read_wave(path, downsample=None, plotData=False):
    with contextlib.closing(wave.open(path, 'rb')) as wf:
        num_channels = wf.getnchannels()
        assert num_channels == 1
        sample_width = wf.getsampwidth()
        assert sample_width == 2
        sample_rate = wf.getframerate()
        assert sample_rate in (8000, 16000, 32000)
        nf = wf.getnframes()
        pcm_data = wf.readframes(nf)

        if plotData:
            pcm_data = np.fromstring(pcm_data, 'Int16')

        if downsample is not None: # this might only work if plotData
            frate = wf.getframerate()
            down_div = frate / downsample
            pcm_data = resample(pcm_data, int(len(pcm_data) / down_div))  # to 1000hz for plotting

        return pcm_data, sample_rate

def write_wave(path, audio, sample_rate):
    with contextlib.closing(wave.open(path, 'wb')) as wf:
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(sample_rate)
        wf.writeframes(audio)

# s = read_wave('/home/schulzet/Projekte/IDEA_projekte/ubrecsys/test_data/activation-log-9b9bc87ccd0bf45-20170516/1494937881487.wav',
#               downsample=1000)
#
# print("l0: ", len(s), " l2: ", len(s))
#
# plt.figure(1)
# plt.title('Signal Wave...')
#
# #s = medfilt(s, kernel_size=5)
# x = np.arange(len(s)) # 16 hz
#
# plt.plot(x, s)
# plt.show()
