#!/usr/bin/python

import webrtcvad
from pyutils.media.audio_u import read_wave
from pyutils.media.voice_activity_detection import detect_va, extract_va
import os

def main():

    file = 'example.wav'
    d = detect_va(file, agressiveness=1, frame_duration_ms=30, padding_duration_ms=300)
    print(d)

    # dir = "out"
    # if not os.path.exists(dir):
    #     os.makedirs(dir)
    #
    # extract_va(file, out_dir=dir, agressiveness=1, frame_duration_ms=30, padding_duration_ms=300)


if __name__ == '__main__':
    main()