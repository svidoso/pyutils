"""
Reading from Java DataInputStream format.

Big endian.
"""

import struct


class DataInputStream:
    def __init__(self, stream):
        self.stream = stream

    def read_boolean(self):
        return struct.unpack('?', self._read(1))[0]

    def read_byte(self):
        return struct.unpack('b', self._read(1))[0]

    def read_unsigned_byte(self):
        return struct.unpack('B', self._read(1))[0]

    def read_char(self):
        return chr(struct.unpack('>H', self._read(2))[0])

    def read_double(self):
        return struct.unpack('>d', self._read(8))[0]

    def read_float(self):
        return struct.unpack('>f', self._read(4))[0]

    def read_short(self):
        return struct.unpack('>h', self._read(2))[0]

    def read_unsigned_short(self):
        return struct.unpack('>H', self._read(2))[0]

    def read_long(self):
        return struct.unpack('>q', self._read(8))[0]

    def read_utf(self):
        utf_length = struct.unpack('>H', self._read(2))[0]
        return self._read(utf_length)

    def read_int(self):
        return struct.unpack('>i', self._read(4))[0]

    def _read(self, n):
        result = self.stream.read(n)
        if len(result) == 0 and n > 0:
            raise EOFError
        return result
