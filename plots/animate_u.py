import numpy as np
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.transforms as mtransforms
import sys

import logging
log = logging.getLogger(__name__)

def animate(x, y, ylabels=None, xlabel="Time [ms]", title=None, fill_intervals=None, styles=None,
            visible_x_len=None, ylim=None, startx=None, stopx=None, fps=None,
            x_padding_right=None, y_padding=0.0, repeat=False,
            fast_fordward_speed=1.0, realtime=False, lock=None,
            save_path=None, verbose=False):
    """
    Blocking! (TODO make option ...)

    Animate x-y data. Supports multiple lines per subplot.
    If x is provided in milliseconds, the animation is time-accurate.
    The xticks are hidden because they cannot be updated without significant performance losses.
    
    :param save_path: save animation as video to provided path; if not None, animation is not visible
    :param x: dimension should be milliseconds

    Structure: list of <arrays or lists containing arrays> 
    Example: x = [[np.array1a, np.array1b], np.array2]
    (can be different size, however len(x[i]) == len(y[i]) is a condition)

    Plot at i contains len(x[i]) lines if x[i] is a list!

    :param y: list of arrays or lists (can be different size, however len(x[i]) == len(y[i]) is a condition)
    :param styles: need the same dimensions as x and y e.g.: styles = [['r-', 'g-', 'y-'], 'r-', 'g-', 'y-'] for a list
    at i=0
    :param ylabels: len(ylabel) == len(y) == len(x)   
    :param xlabel:
    :param fill_intervals: list of 4D arrays; [a, b, color, alpha]
    :param visible_x_len: 
    :param ylim: len(ylim) == len(y) == len(x) | some elements can be None meaning that the interval is calculated from the data
    :param startx: 
    :param stopx: 
    :param fps: 
    :param x_padding_right: 
    :param y_padding: obsolete if ylim is provided
    :param repeat: 
    :param realtime: will always show the last values, provide lock for concurrency, runs forever
    :param lock: 
    :param title: 
    :param fast_fordward_speed:

    :return: Nothing

    """
    if len(x) == 0 or len(y) == 0 or len(x[0]) == 0 or len(y[0]) == 0:
        raise ValueError("Not enough data provided!")

    if isinstance(x[0], list):
        if startx is None:
            startx = x[0][0][0]
        if stopx is None:
            stopx = x[0][0][-1]
    else:
        if startx is None:
            startx = x[0][0]
        if stopx is None:
            stopx = x[0][-1]

    if fps is None:
        fps = 25.0
    fps = float(fps)
    if visible_x_len is None:
        visible_x_len = 5000  # shows 5 seconds
    if x_padding_right is None:
        x_padding_right = 100

    if styles is None:  # make styles array
        styles = []
        for a in x:
            if isinstance(a, list):
                lstyles = []
                for _ in a:
                    lstyles.append("")
                styles.append(lstyles)
            else:
                styles.append("")

    if 1000 % fps != 0:
        log.warning("Better choose fps such that 1000 % fps == 0, otherwise the delay between data-sources will slowly increase!")

    wait_interval = 1000.0 / float(fps)  # millis

    frame_cnt = (stopx - startx) / wait_interval / fast_fordward_speed
    fig, axes = plt.subplots(nrows=len(x), sharex=True, sharey=False)

    if not isinstance(axes, collections.Iterable):  # if there is only one plot
        axes = [axes]

    plt.xlabel(xlabel)

    plt.xlim([startx, startx + visible_x_len])

    timertext = axes[-1].text(0.95, 0.1, 'matplotlib',
                            horizontalalignment = 'center',
                            verticalalignment = 'center',
                            size=10, # fontsize
                            transform = axes[-1].transAxes)

    axes[-1].tick_params( # hide xticks
        axis='x',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom='off',  # ticks along the bottom edge are off
        top='off',  # ticks along the top edge are off
        labelbottom='off')

    lines = []
    rects = []

    # determine y-limits if not provided
    if ylim is None:
        ylim = []
    ylim2 = ylim
    for l in np.arange(len(axes)):
        if len(ylim2) == l:
            ylim2.append(None)

        if ylim2[l] is None:
            if isinstance(y[l], list):  # find y-limits for current plot
                tylim = [np.ndarray.min(y[l][0]), np.ndarray.max(y[l][0])]
                for i in range(1, len(y[l])):
                    tylim = [min(np.ndarray.min(y[l][i]), tylim[0]), max(np.ndarray.max(y[l][i]), tylim[1])]
                ylim2[l] = tylim
            else:
                ylim2[l] = [np.ndarray.min(y[l]), np.ndarray.max(y[l])]


    for l, (ax, style) in enumerate(zip(axes, styles)):
        if l == 0 and title is not None:
            ax.set_title(title)
        ax.set_ylim([ylim2[l][0] - y_padding, ylim2[l][1] + y_padding])

        if ylabels is not None:
            ax.set_ylabel(ylabels[l])

        # make filled areas
        if fill_intervals is not None:
            trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
            for fi in fill_intervals:
                d = [fi[0], fi[1]]
                #print(ylim2[l][0], ylim2[l][1])
                rects.append(ax.fill_between(d, ylim2[l][0]-y_padding, ylim2[l][1]+y_padding + 1,  facecolor=fi[2], # +1 for bug ...
                                             alpha=fi[3], transform=trans))

        # make lines
        li = []
        if isinstance(x[l], list):
            for xi, yi, s in zip(x[l], y[l], style):
                li.append(ax.plot(xi, yi, s, animated=True)[
                              0])  # if xticks are not changed, "animated=True" improves performance
        else:
            li = ax.plot(x[l], y[l], style, animated=True)[
                0]  # if xticks are not changed, "animated=True" improves performance
        lines.append(li)

    if verbose:
        log.info("FPS: " + str(fps))
        log.info("Duration: " + str((stopx - startx)/1000.0) + "s")
        if not realtime:
            log.info("Framecount: " + str(frame_cnt))
        log.info("Wait between frames: " + '{:10.2f}'.format(wait_interval) + "ms")
        log.info("Number of plots: " + str(len(x)))

    def animate(i):
        non_packed = []

        xmax = i * wait_interval * fast_fordward_speed + startx
        xmin = xmax - visible_x_len

        # non_packed.append(axes[-1].xaxis)
        timertext.set_text(str(int(xmax/1000)) + "s")
        #timertext.set_position((xmax, ylim2[1]))
        non_packed.append(timertext)

        for j, line in enumerate(lines, start=0):
            if isinstance(line, list):
                for i in range(len(line)):

                    if lock is not None:
                        lock.acquire(True)

                    if realtime:
                        xmax = x[j][i][-1]
                        xmin = xmax - visible_x_len

                    xj = x[j][i]
                    yj = y[j][i]
                    b = (xj <= xmax) & (xj >= xmin)

                    line[i].set_data(xj[b], yj[b])

                    if lock is not None:
                        lock.release()

                    non_packed.append(line[i])
            else:

                if lock is not None:
                    lock.acquire(True)

                if realtime:
                    xmax = x[j][-1]
                    xmin = xmax - visible_x_len

                xj = x[j]
                yj = y[j]
                b = (xj <= xmax) & (xj >= xmin)

                line.set_data(xj[b], yj[b])

                if lock is not None:
                    lock.release()

                non_packed.append(line)

        for ax in axes:
            ax.set_xlim(max(xmin, startx), max(xmax, startx + visible_x_len) + x_padding_right)

        for r in rects:
            non_packed.append(r)

        return non_packed

    if realtime:
        frame_cnt = sys.maxsize

    ani = animation.FuncAnimation(fig, animate,
                                  frames=int(frame_cnt),
                                  interval=wait_interval,
                                  blit=True,
                                  repeat=repeat)

    if save_path is not None:
        ani.save(save_path, fps=30.0, extra_args=['-vcodec', 'libx264'])

    plt.show()


# def _blit_draw(self, artists, bg_cache):
#     # Handles blitted drawing, which renders only the artists given instead
#     # of the entire figure.
#     # http://stackoverflow.com/questions/17558096/animated-title-in-matplotlib/30860027#30860027
#     # This function is for an animated xaxis
#     updated_ax = []
#     for a in artists:
#         # If we haven't cached the background for this axes object, do
#         # so now. This might not always be reliable, but it's an attempt
#         # to automate the process.
#         if a.axes not in bg_cache:
#             # bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.bbox)
#             # change here
#             bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.figure.bbox)
#         a.axes.draw_artist(a)
#         updated_ax.append(a.axes)
#
#     # After rendering all the needed artists, blit each axes individually.
#     for ax in set(updated_ax):
#         # and here
#         # ax.figure.canvas.blit(ax.bbox)
#         ax.figure.canvas.blit(ax.figure.bbox)
#
#
# # MONKEY PATCH!!
# matplotlib.animation.Animation._blit_draw = _blit_draw
