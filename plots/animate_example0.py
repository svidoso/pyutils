from pyutils.plots.animate_u import animate
import numpy as np

x = [np.arange(0,100000, 100)]
y = [np.random.random_sample(1000)]

for l in x:
    print("x: " + str(isinstance(l[0], list)))

for l in y:
    print("y: " + str(isinstance(l[0], list)))

animate(x,y)
