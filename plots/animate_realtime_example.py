#!/usr/bin/python2

import sys
print(sys.version_info)
try:
    import _thread
    thread_ = _thread
except ImportError:
    import thread
    thread_ = thread

import time
from pyutils.plots.animate_u import animate
import numpy as np
from numpy.random import normal

rate = 30.0
ms_step = int(1.0/rate * 1000)
noise_std = 0.1
width = 10000

lock = thread_.allocate_lock() # thread.RLock()

x = [np.arange(0, width, ms_step), np.arange(0, width, ms_step)]
y = [normal(np.sin(x[0]/1000.0),noise_std), normal(np.cos(x[1]/1000.0),noise_std*4)]

ylables = ["sin gaussian_noise\nsigma=" + str(noise_std), "cos gaussian_noise\nsigma=" + str(noise_std*4)]

# Define a function for the thread
def add_to_queue(x, y, lock):
    global ms_step
    while 1:

        lock.acquire(True)
        x[0] = np.arange(x[0][0] + ms_step, x[0][-1] + 2*ms_step, ms_step)
        x[1] = np.arange(x[1][0] + ms_step, x[1][-1] + 2*ms_step, ms_step)

        ytemp = np.append(y[0][1:], [normal(np.sin(x[0][-1]/1000.0), noise_std)])
        y[0] = ytemp

        ytemp = np.append(y[1][1:], [normal(np.cos(x[1][-1]/1000.0), noise_std*4)])
        y[1] = ytemp

        #print(y[1].shape, y[1][-1], x[1].shape, x[1][-1])

        lock.release()

        time.sleep(1.0/rate)


# Create two threads as follows
try:
    thread_.start_new_thread(add_to_queue, (x, y, lock,))
except:
    print("Error: unable to start thread")

animate(x, y, visible_x_len=width-1000, realtime=True, lock=lock, ylabels=ylables)
