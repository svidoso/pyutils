import numpy as np
from pyutils.plots.animate_u import animate

start_stamp = 1000
stop_stamp = 30000

ylim = []

x = [np.arange(start_stamp, stop_stamp, 1), np.arange(start_stamp, stop_stamp, 10),
     np.arange(start_stamp, stop_stamp, 100)]
y = [np.sin(x[0] / 1000), np.cos(x[1] / 1000), np.tan(x[2] / 1000)]

ylim.append(None)
ylim.append(None)
ylim.append(None)
ylim.append([-1,2])

x = [x.copy()] + x
y = [y.copy()] + y

yl = ["3", "asd", "qwe", "dfg"]

styles = [['r-', 'g-', 'y-'], 'r-', 'g-', 'y-']

finter = [[0, 3000, 'green', 0.1], [12500, 15000, 'red', 0.3]]

animate(x, y, visible_x_len=10000, fill_intervals=finter,
                  styles=styles, ylabels=yl,
                  y_padding=0.3, x_padding_right=1000,
                  fast_fordward_speed=1.0,
                  stopx=stop_stamp, fps=30,
                    ylim=ylim)
