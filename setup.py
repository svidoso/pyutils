#!/usr/bin/env python
try: 
    from setuptools import setup, find_packages
except ImportError: 
    from distutils.core import setup

setup(
    name='pyutils',
    version='0.4.3',
    package_dir={'pyutils':'', 'pyutils.plots':'plots', 'pyutils.media':'media', 'pyutils.system':'system', 'pyutils.time':'time'},
    packages=['pyutils', 'pyutils.plots', 'pyutils.media', 'pyutils.system', 'pyutils.time'],
    url='svidoso.com',
    license='all_mine',
    author='schulzet',
    author_email='svidoso@svidoso.com',
    description='collection of my python code',
    install_requires=[
          'matplotlib', 'webrtcvad', 'numpy', 'wave', 'scipy', 'subprocess32;python_version<"3.0"'
      ],
    zip_safe=False, # generating problems otherwise
)
